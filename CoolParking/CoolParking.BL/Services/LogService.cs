﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private static readonly object lockObj = new object();
        public string LogPath { get; private set; }

        public LogService(string filePath)
        {
            LogPath = filePath;
        }

        public string Read()
        {
            lock(lockObj)
            {
                return File.ReadAllText(LogPath);
            }
        }

        public void Write(string logInfo)
        {
            lock(lockObj)
            {
                using (var stream = File.AppendText(LogPath))
                {
                    stream.WriteLine(logInfo);
                }
            }
        }
    }
}