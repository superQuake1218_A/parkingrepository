﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }

        internal void ChangeBalance(decimal sumDiff)
        {
            Balance += sumDiff;
        }

        public Vehicle()
        {

        }

        public Vehicle(string id, VehicleType vehicletype, decimal balance)
        {
            if( !IsValidBalance(balance)|| !IsValidId(id) )
            {
                throw new ArgumentException("Vehicle negative balance");
            }
            Id = id;
            VehicleType = vehicletype;
            Balance = balance;
        }

        public static bool IsValidId(string VehicleId)
        {
            if(VehicleId == null)
            {
                return false;
            }

            Regex regex = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");
            return regex.IsMatch(VehicleId);
        }

        public static bool IsValidBalance(decimal balance)
        {
            return balance >= 0;
        }
    }
}