﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    internal class Settings
    {
        public decimal StartParkingBalance { get; set; }
        public decimal PenaltyRatio { get; set; }
        public int PlacesCount { get; set; }
        public TimeSpan BillingPeriod { get; set; }
        public TimeSpan LoggingPeriod { get; set; }
        public RateSettings RateSettings { get;  set; }

        public Settings()
        {
            RateSettings = new RateSettings();
        }
    }
}

