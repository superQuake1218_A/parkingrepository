﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Models
{
    internal class Parking
    {
        public decimal Balance { get; set; }
        private readonly ConcurrentDictionary<string, Vehicle> vehicles;
        private static readonly object lockObj = new object();
        public static Settings Settings { get; set; }

        private static readonly Lazy<Parking> instance = new Lazy<Parking>(() => new Parking());

        public static Parking Instance
        {
            get
            {
                return instance.Value;
            }
        }

        private Parking()
        {
            vehicles = new ConcurrentDictionary<string, Vehicle>();
            Balance = Settings?.StartParkingBalance??0;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            bool isSuccess = false;
            lock(lockObj)
            {
                if (GetFreePlaces() <= 0)
                {
                    throw new InvalidOperationException("Has no free places!");
                }

                isSuccess = this.vehicles.TryAdd(vehicle.Id, vehicle);
            }

            if(!isSuccess)
            {
                throw new ArgumentException("Vehicle does not added!");
            }
        }

        public void Clear()
        {
            lock(lockObj)
            {
                vehicles.Clear();
                Balance = 0;
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            try
            {
                Vehicle vehicle;
                this.vehicles.TryRemove(vehicleId,out vehicle);
            }
            catch(KeyNotFoundException ex)
            {
                throw new ArgumentException("Vehicle with this id not founded", ex);
            }
        }

        public Vehicle GetVehicle(string vehicleId)
        {
            try
            {
                return vehicles[vehicleId];
            }
            catch (KeyNotFoundException ex)
            {
                throw new ArgumentException("Vehicle with this id not founded", ex);
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            lock(lockObj)
            {
                GetVehicle(vehicleId).ChangeBalance(sum);
            }
        }

        public int GetCapacity()
        {
            return Settings.PlacesCount;
        }

        public int GetBusyPlaces()
        {
            return vehicles.Count;
        }

        public int GetFreePlaces()
        {
            lock(lockObj)
            {
                return GetCapacity() - GetBusyPlaces();
            }
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return vehicles.Values.ToList().AsReadOnly();
        }
    }
}
