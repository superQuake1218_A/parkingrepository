﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Text.Json.Serialization;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; private set; }

        [JsonPropertyName("sum")]
        public decimal Sum { get; private set; }

        [JsonPropertyName("transactionDate")]
        public DateTime Time { get; private set; }

        public TransactionInfo(DateTime time, string vehicleId, decimal sum)
        {
            Time = time;
            VehicleId = vehicleId;
            Sum = sum;
        }
    }
}
