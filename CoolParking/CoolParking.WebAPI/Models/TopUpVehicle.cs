﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class TopUpVehicle: IValidatableObject
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        public decimal Sum { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!Vehicle.IsValidId(Id))
            {
                yield return new ValidationResult($"Incorrect id:{Id}", new[] { nameof(Id) });
            }

            if (!Vehicle.IsValidBalance(Sum))
            {
                yield return new ValidationResult($"Negative balance :{Sum}", new[] { nameof(Sum) });
            }
        }
    }
}
