﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class VehicleValidated : Vehicle, IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(!IsValidId(Id))
            {
                yield return new ValidationResult($"Incorrect id:{Id}", new[] { nameof(Id)});
            }

            if (!IsValidBalance(Balance))
            {
                yield return new ValidationResult($"Negative balance :{Balance}", new[] { nameof(Balance) });
            }

            if(!Enum.IsDefined(typeof(VehicleType), VehicleType))
            {
                yield return new ValidationResult($"Invalid vehicle type value :{VehicleType}", new[] { nameof(VehicleType) });
            }
        }
    }
}
