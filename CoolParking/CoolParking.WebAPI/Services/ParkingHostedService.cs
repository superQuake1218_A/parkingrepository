﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using Microsoft.Extensions.Logging;
using System.Collections.ObjectModel;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Services
{
    public class ParkingHostedService : IHostedService, IDisposable
    {
        private readonly ParkingService _parkingService;
        
        public ParkingHostedService(IParkingService parkingService)
        {
            _parkingService = parkingService as ParkingService;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _parkingService.StartService();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _parkingService?.StopService();
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _parkingService?.Dispose();
        }
    }
}
