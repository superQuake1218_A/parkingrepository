using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using Microsoft.Extensions.DependencyInjection;
using CoolParking.WebAPI.Services;

namespace CoolParking.WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureServices(services =>
                {
                    services.AddTransient<ITimerService, TimerService>();

                    services.AddSingleton<ILogService, LogService>((ctx)=> {
                        return new LogService($@"{AppDomain.CurrentDomain.BaseDirectory}Transactions.log");
                    });
                    
                    services.AddSingleton<IParkingService,ParkingService>((ctx)=> {
                        ITimerService withdrawTimer = ctx.GetService<ITimerService>();
                        ITimerService logTimer = ctx.GetService<ITimerService>();
                        ILogService logService = ctx.GetService<ILogService>();

                        ParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);
                        parkingService.StopService();

                        return parkingService;
                    });

                    services.AddHostedService<ParkingHostedService>();
                });
    }
}
