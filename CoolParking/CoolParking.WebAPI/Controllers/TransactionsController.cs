﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Models;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        // GET: api/Transactions/last
        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionInfo>> GetLastTransactions()
        {
            return _parkingService.GetLastParkingTransactions();
        }

        // GET: api/Transactions/all
        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            string log;
            try
            {
                log = _parkingService.ReadFromLog();
            }
            catch
            {
                return NotFound();
            }
            return log;
        }

        // PUT: api/Transactions/5
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> PutTopUpVehicle([FromBody] TopUpVehicle topUpVehicle)
        {
            var vehicle = _parkingService.GetVehicles().Where(v => v.Id == topUpVehicle.Id).SingleOrDefault();

            try
            {
                _parkingService.TopUpVehicle(topUpVehicle.Id, topUpVehicle.Sum);
            }
            catch
            {
                if(vehicle == null)
                {
                    return NotFound();
                }
                return BadRequest();
            }

            return _parkingService.GetVehicles().Where(v => v.Id == topUpVehicle.Id).Single();
        }
    }
}
