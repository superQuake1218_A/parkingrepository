﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        // GET: api/Vehicles
        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> Get()
        {
            return _parkingService.GetVehicles();
        }

        // GET: api/Vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpGet("{id}", Name = "Get")]
        public ActionResult<Vehicle> Get(string id)
        {
            if(!Vehicle.IsValidId(id))
            {
                return BadRequest();
            }
            var vehicle = _parkingService.GetVehicles().Where(v => v.Id == id).FirstOrDefault();
            if(vehicle != null)
            {
                return vehicle;
            }

            return NotFound();
        }

        // POST: api/Vehicles
        [HttpPost]
        public ActionResult Post([FromBody] VehicleValidated vehicle)
        {
            try
            {
                _parkingService.AddVehicle(vehicle);
            }
            catch
            {
                return BadRequest();
            }
            return Created(HttpContext.Request.GetEncodedUrl(), vehicle);
        }

        // DELETE: api/Vehicles/5
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            if(!Vehicle.IsValidId(id))
            {
                return BadRequest();
            }

            try
            {
                _parkingService.RemoveVehicle(id);
            }
            catch
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
