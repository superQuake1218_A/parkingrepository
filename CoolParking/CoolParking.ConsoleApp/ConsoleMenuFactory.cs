﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoolParking.ConsoleApp
{
    class ConsoleMenuFactory
    {
        private IParkingService parkingService;

        public ConsoleMenuFactory(IParkingService parkingService)
        {
            if(parkingService == null)
            {
                throw new ArgumentException("Parking service reference is null");
            }
            this.parkingService = parkingService;
        }

        public ConsoleMenu CreateConsoleMenu()
        {
            ConsoleMenu vehicleTypes = CreateVehicleTypeMenu();

            ConsoleMenu cm = new ConsoleMenu("Main menu", null,
                                    new ConsoleMenu("Show current parking balance", ShowCurrentBalance),
                                    new ConsoleMenu("Number of the free space",ShowFreeSpace),
                                    new ConsoleMenu("Show all transactions", ShowAllTransactions),
                                    new ConsoleMenu("Show transaction history", ShowTransactionsHistory),
                                    new ConsoleMenu("Show all vehicles on parking", ShowAllVehicles),
                                    new ConsoleMenu("Add vehicle on parking", null, vehicleTypes),
                                    new ConsoleMenu("Remove vehicle from parking", ShowRemoveVehicleDialog),
                                    new ConsoleMenu("Top up vehicle", ShowTopUpVehicleDialog));
            return cm;
        }

        private ConsoleMenu CreateVehicleTypeMenu()
        {
            ConsoleMenu vehicleTypeMenu = new ConsoleMenu("Select vehicle type", null);
            foreach(var vt in Enum.GetValues(typeof(VehicleType)))
            {
                VehicleType vehicleType = (VehicleType)vt;
                vehicleTypeMenu.AddSubmenus(new ConsoleMenu($"{vehicleType:G}", ()=> { AddVehicleToParking(vehicleType); }));
            }
            return vehicleTypeMenu;
        }

        private void ShowCurrentBalance()
        {
            Console.WriteLine($"Current parking balance is: {parkingService.GetBalance()}");
        }

        private void ShowFreeSpace()
        {
            Console.WriteLine($"Free places: {parkingService.GetFreePlaces()} of {parkingService.GetCapacity()}");
        }

        private void ShowAllTransactions()
        {
            var transactions = parkingService.GetLastParkingTransactions();
            Console.WriteLine($"Transacions count: {transactions.Length}");
            foreach (var tr in transactions)
            {
                Console.WriteLine($"{tr.Time} {tr.VehicleId} {tr.Sum}");
            }
        }

        private void ShowTransactionsHistory()
        {
            Console.WriteLine("Transacions history:");
            Console.WriteLine($"{parkingService.ReadFromLog()}");
        }

        private void ShowAllVehicles()
        {
            var vehicles = parkingService.GetVehicles();
            foreach(var vehicle in vehicles)
            {
                Console.WriteLine($"Type:{vehicle.VehicleType:G} Number:{vehicle.Id} Balance:{vehicle.Balance}");
            }
        }

        private void AddVehicleToParking(VehicleType vehicleType)
        {
            bool isReadId = ReadVehicleId(out string vehicleId);
            if(!isReadId)
            {
                return;
            }

            bool isReadBalance = ReadVehicleBalance(out decimal balance);
            if (!isReadBalance)
            {
                return;
            }

            parkingService.AddVehicle(new Vehicle(vehicleId, vehicleType, balance));

            Console.WriteLine($"Vehicle :{vehicleType:G} {vehicleId} {balance} added to parking!");
        }

        private bool ReadVehicleId(out string vehicleId)
        {
            int attempts = 3;
            vehicleId = "";

            Console.WriteLine("Enter vehicle id in format XX-YYYY-XX or \"0\" to cancel:");
            var usermsg = Console.ReadLine();
            while(!Vehicle.IsValidId(usermsg))
            {
                --attempts;
                bool cancel = ((Int32.TryParse(usermsg, out int num) && num == 0))|| (attempts <= 0);
                if (cancel)
                {
                    return false;
                }
                Console.WriteLine("Previous id is not correct. Enter correct vehicle id in format XX-YYYY-XX or \"0\" to cancel:");
                usermsg = Console.ReadLine();
            }
            vehicleId = usermsg;
            return true;
        }

        private bool ReadVehicleBalance(out decimal balance)
        {
            int attempts = 3;
            Console.WriteLine("Enter vehicle balance:");
            while (!decimal.TryParse(Console.ReadLine(), out balance) && Vehicle.IsValidBalance(balance))
            {
                --attempts;
                if (attempts <= 0)
                {
                    return false;
                }
                Console.WriteLine("Enter correct vehicle balance!");
            }
            return true;
        }

        private void ShowRemoveVehicleDialog()
        {
            bool isReadId = ReadVehicleId(out string vehicleId);
            if (!isReadId)
            {
                return;
            }

            try 
            {
                parkingService.RemoveVehicle(vehicleId);
            }
            catch(ArgumentException)
            {
                Console.WriteLine("Vehicle not found or balance is negative!");
                return;
            }

            Console.WriteLine($"Vehicle with id: {vehicleId} removed");
        }

        private void ShowTopUpVehicleDialog()
        {
            bool isReadId = ReadVehicleId(out string vehicleId);
            if (!isReadId)
            {
                return;
            }

            bool isReadBalance = ReadVehicleBalance(out decimal sum);
            if (!isReadBalance)
            {
                return;
            }

            parkingService.TopUpVehicle(vehicleId, sum);
            var vehicle = parkingService.GetVehicles().Where(v => v.Id == vehicleId).SingleOrDefault();
            Console.WriteLine($"Vehicle with id: {vehicleId} has been added sum {sum}. Current balance is: {vehicle.Balance}");
        }
    }
}
