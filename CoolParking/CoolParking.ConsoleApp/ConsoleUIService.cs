﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.ConsoleApp
{
    class ConsoleUIService
    {
        public static void Show(ConsoleMenu consoleMenu)
        {
            ConsoleMenu currentMenu = consoleMenu;
            while(currentMenu != null)
            {
                Console.WriteLine(currentMenu.ToString());

                int menuItemNumber;
                if(!ReadMenuItemNumber(out menuItemNumber))
                {
                    continue;
                }

                try
                {
                    currentMenu = currentMenu.GetSubmenuByNumber(menuItemNumber);
                    currentMenu?.Invoke();
                }
                catch(ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static bool ReadMenuItemNumber(out int menuItemNumber)
        {
            int attempts = 3;
            Console.WriteLine("Select menu item by number:");
            while (!Int32.TryParse(Console.ReadLine(), out menuItemNumber))
            {
                Console.WriteLine("Please enter number");
                --attempts;
                if(attempts <=0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
