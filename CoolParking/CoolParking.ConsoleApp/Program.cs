﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;

namespace CoolParking.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            TimerService withdrawTimer = new TimerService();
            TimerService logTimer = new TimerService();
            LogService logService = new LogService($@"{AppDomain.CurrentDomain.BaseDirectory}Transactions.log");

            ParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);

            var menu = new ConsoleMenuFactory(parkingService).CreateConsoleMenu();
            ConsoleUIService.Show(menu);

            Console.ReadKey(true);
        }
    }
}
